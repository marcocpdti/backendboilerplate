module.exports = {
	database: {
		development: {
			username: "user",
			password: "pass",
			database: "backendschedule",
			host: "127.0.0.1",
			// port: 3360,
			dialect: "mysql",
			operatorsAliases: false,
			logging: false,
			pool: {
				max: 5,
				min: 0,
				acquire: 30000,
				idle: 10000
			}
		},
		production: {
			username: "user",
			password: "pass",
			database: "backendschedule",
			host: "127.0.0.1",
			// port: 3360,
			dialect: "mysql",
			operatorsAliases: false,
			logging: false,
			pool: {
				max: 5,
				min: 0,
				acquire: 30000,
				idle: 10000
			}
		}
	},
}
