const gulp = require("gulp");
const del = require('del');
const babel = require("gulp-babel");
const jshint = require('gulp-jshint');
const stylehint = require('jshint-stylish');
const nodemon = require('gulp-nodemon');
const run = require('gulp-run');
const pm2 = require('pm2');
const config = require('./config/config.js');

var ext_replace = require('gulp-ext-replace');
var replace = require('gulp-string-replace');
var conflict = require("gulp-conflict");
var execSync = require('child_process').execSync;

console.log('teste começou gulpfile')

gulp.task('clean', (done) => {
    return del('dist');
});

console.log('teste começou gulpfile 1')

gulp.task("babel",  function(done) {
    return gulp.src(["./src/app.js", "./src/**/*.js"])
      .pipe(babel())
      .pipe(gulp.dest("dist"));
});

console.log('teste começou gulpfile 2')

gulp.task("jsonFiles",  function(done) {
    return gulp.src(["./src/**/*.json"])
      .pipe(gulp.dest("dist"));
});

console.log('teste começou gulpfile 3')

gulp.task("build", gulp.series('clean', 'babel', 'jsonFiles'));

gulp.task('lint', function(done) {
    return gulp.src('./src/**/*.js')
        .pipe(jshint({ "esversion":8, "node": true}))
        .pipe(jshint.reporter(stylehint));    
});

gulp.task('install', function (done) {
    let response = execSync(`npm install`).toString();
    return done();
})

console.log('teste começou gulpfile 4')

gulp.task('nodemon', function (done) {
    return nodemon({
        script: './dist/server.js',
        env: { 'NODE_ENV': 'development', 'DEBUG': 'server,info,warn,error,dev' },
        done: done,
        tasks: ['rebuild'],
        delay: 1500,
        ignore: './public'        
    })
})

gulp.task('killport', function(done){

    let response
    let pid
    try{
        response = execSync(`fuser ${(config.server.development.port)}/tcp`);
        pid = parseInt(response)
    }catch(e){
        return done()
    }
  
    if(pid && isNaN(pid))
        return done()

    execSync(`kill ${pid}`).toString()

    return done()

})

console.log('teste começou gulpfile 5')

gulp.task('rebuild',  gulp.series('killport','lint', 'build'));

console.log('teste começou gulpfile 6')

gulp.task('runmodel', function(done){
    console.log('TESTE RUN MODEL GULP FILE')
    let response = execSync(`NODE_ENV=development node ./dist/util/baseToModel.js`).toString()
    console.log(response)
    done();
})

gulp.task('prod',  gulp.series('install', 'build', 'runmodel'))

gulp.task('start', gulp.series('killport', 'clean', 'lint', 'build', 'nodemon'));

gulp.task('create-teste', () => {
    return gulp.src('./dist/**/*.js')
    .pipe(ext_replace('.spec.js'))
    .pipe(conflict('./spec', {defaultChoice: 'n'}))
    .pipe(replace(new RegExp('.|\n', 'g'), '', {logs: { enabled: false }}))
    .pipe(gulp.dest('./spec'))
});
