import * as Yup from 'yup';

import db from '../models';

class BairrosController {

    //função criar bairro
    async create(req, res) {

        const schema = Yup.object().shape({
            bairro: Yup.string().required(),
        });

        if (!(await schema.isValid(req.body))){
            return res.status(400).json({error: 'Validation fails'});
        }

        const bairroExists = await db.bairrosMorrinhos.findOne({ where: { bairro: req.body.bairro } });

        if (bairroExists) {
            return res.status(400).json({ error: "Bairro já existe!" });
        }

        const { idbairro, bairro } = await db.bairrosMorrinhos.create(req.body);

        return res.json({ idbairro, bairro });
    }

    //função para listar todos os bairros
    async getAll(req, res) {
        const allBairros = await db.bairrosMorrinhos.findAll();
        return res.json(allBairros);
    }

    //função atualizar dados do bairro
    async update(req, res) {

        const schema = Yup.object().shape({
            idbairro: Yup.string().required(),
            bairro: Yup.string().required()
        });

        if (!(await schema.isValid(req.body))){
            return res.status(400).json({error: 'Validation fails'});
        }

        const response = await db.bairrosMorrinhos.update(req.body, {where: {idbairro: req.body.idbairro}});

        if(response == 1 ){
            return res.status(200).json('Dados atualizados com sucesso!');
        }else{
            return res.status(400).json({erro: "erro ao atualizar os dados no banco"});
        }

    }

}

export default new BairrosController();