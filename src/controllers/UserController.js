import * as Yup from 'yup';
import bcrypt from "bcryptjs";

import db from '../models';
import checkPassword from '../util/checkPassword';

class UserController {

    //função criar usuario
    async create(req, res) {

        const schema = Yup.object().shape({
            user_name: Yup.string().required(),
            user_email: Yup.string().email().required(),
            password: Yup.string().required(),
            user_permission: Yup.string().required(),
        });

        if (!(await schema.isValid(req.body))){
            return res.status(400).json({error: 'Validation fails'});
        }

        const userExists = await db.users.findOne({ where: { user_email: req.body.user_email } });

        if (userExists) {
            return res.status(400).json({ error: "User already exists!" });
        }

        req.body.password_hash = await bcrypt.hash(req.body.password, 8);

        const { id_user, user_name, user_email, password_hash, user_permission } = await db.users.create(req.body);

        return res.json({ id_user, user_name, user_email, password_hash, user_permission });
    }

    //função para listar todos os usuários
    async getAll(req, res) {
        const allUsers = await db.users.findAll();
        return res.json(allUsers);
    }
    
    //função atualizar dados do usuario
    async update(req, res) {

        const schema = Yup.object().shape({
            user_name: Yup.string(),
            user_email: Yup.string().email(),
            oldPassword: Yup.string(),
            password: Yup.string().min(6).when('oldPassword', (oldPassword, field)=> 
                oldPassword ? field.required() : field
            ),
            confirmPassword: Yup.string().when('password', (password, field) => 
                password ? field.required().oneOf([Yup.ref('password')]): field
            ),
            user_permission: Yup.string().required(),
        });

        if (!(await schema.isValid(req.body))){
            return res.status(400).json({error: 'Validation fails'});
        }

        const user = await db.users.findByPk(req.userId);

        if (req.body.user_email !== user.user_email) {
            const userExists = await db.users.findOne({ where: { user_email: req.body.user_email } });
            if (userExists) {
                return res.status(400).json({ error: "User already exists!" });
            }
        }

        if (req.body.oldPassword && !(await checkPassword(req.body.oldPassword, user.password_hash))) {
            return res.status(401).json({ error: "Password does not match " });
        }

        req.body.password_hash = await bcrypt.hash(req.body.password, 8);

        const response = await db.users.update(req.body, {where: {id_user: req.userId}});

        // return res.json({ id_usuarios, nome_usuarios, email_usuarios });
        if(response == 1 ){
            return res.status(200).json('Dados atualizados com sucesso!');
        }else{
            return res.status(400).json({erro: "erro ao atualizar os dados no banco"});
        }

    }

}

export default new UserController();