const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
import {associate} from './associate';

const basename = path.basename(module.filename);
const env = process.env.NODE_ENV || 'development';
const config = require('../../config/config.js').database[env];

let db = null;

if (!db) {

    db = {};

    const sequelize = new Sequelize(config.database, config.username, config.password, {
        host: config.host,
        // port: config.port,
        dialect: config.dialect,
        operatorsAliases: config.operatorsAliases,
        logging: config.logging,
        pool: config.pool
    });

    let teste = fs.readdirSync(__dirname)
        .filter((file) => {
            const fileSlice = file.slice(-3);
            return (file.indexOf('.') !== 0) && (file !== basename) && ((fileSlice === '.js') && file !== 'associate.js' && file !== 'dbprod.js');
        })
        .forEach((file) => {

            const model = sequelize.import(path.join(__dirname, file));

            let objetoProps = {};

            Object.keys(model.tableAttributes).forEach(item=>{
                objetoProps[item] = {
                    values: model.tableAttributes[item].values ? model.tableAttributes[item].values : [],
                    allowNull: model.tableAttributes[item].allowNull ? model.tableAttributes[item].allowNull : false,
                    primaryKey: model.tableAttributes[item].primaryKey ? model.tableAttributes[item].primaryKey : false,
                    autoIncrement: model.tableAttributes[item].autoIncrement ? model.tableAttributes[item].autoIncrement : false,
                    defaultValue: model.tableAttributes[item].defaultValue
                }
            })

            model.objetoProps = objetoProps;

            db[file.split('.')[0]] = model;

        });

    db.sequelize = sequelize;

    associate(db);
}

module.exports = db;
