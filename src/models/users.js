/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('users', {
		id_user: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			allowNull: false,
			primaryKey: true
		},
		user_name: {
			type: DataTypes.STRING(100),
			allowNull: false
		},
		user_email: {
			type: DataTypes.STRING(60),
			allowNull: false,
			unique: true
		},
		password_hash: {
			type: DataTypes.STRING(150),
			allowNull: false
		},
		user_permission: {
			type: DataTypes.ENUM('admin','comum','cad'),
			allowNull: false,
			defaultValue: 'comum'
		},
		createdAt: {
			type: DataTypes.DATEONLY,
			allowNull: true
		},
		updatedAt: {
			type: DataTypes.DATEONLY,
			allowNull: true
		}
	}, {
		tableName: 'users'
	});
};
