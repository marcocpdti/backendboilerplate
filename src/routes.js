import { Router } from 'express';
import userController from './controllers/UserController';
import sessionController from './controllers/SessionController';
import bairrosController from './controllers/BairrosController';


import AuthMiddleware from './middlewares/auth';

const routes = new Router();

routes.get('/', (req, res) => {
    return res.json({message: 'Hello World!'});
});

routes.post('/users', userController.create);
routes.post("/sessions", sessionController.store);

routes.use(AuthMiddleware);
routes.get('/users', userController.getAll);
routes.put('/users', userController.update);
routes.post('/bairros', bairrosController.create);
routes.get('/bairros', bairrosController.getAll);
routes.put('/bairros', bairrosController.update);

export default routes;